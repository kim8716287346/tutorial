Welcome to your tutorial repository!
================
Learn how to use Git and Bitbucket with either SourceTree, one of the best Git clients available, or using Git from the command line. Whichever you choose you will learn how set up Git, clone this repository locally. Then learn how to make and commit a change locally and push that change back to Bitbucket.

____

WordPress
From Wikipedia, the free encyclopedia
This article is about the blogging software. For the blog host, see WordPress.com.
WordPress WordPress logo.svg
WordPress MP6 dashboard.png
WordPress Dashboard
Developer(s) 	WordPress Foundation
Initial release 	May 27, 2003; 12 years ago[1]
Stable release 	4.3.1 (September 15, 2015; 38 days ago) [±][2]
Preview release 	4.4 Beta 1 (October 22, 2015; 1 day ago) [±][3]
Development status 	Active
Operating system 	Cross-platform
Platform 	PHP
Type 	Blog software, Content Management System, Content Management Framework
License 	GNU GPLv2+[4]
Website 	wordpress.org

WordPress is a free and open-source content management system (CMS) based on PHP and MySQL.[5] Features include a plugin architecture and a template system. WordPress was used by more than 23.3% of the top 10 million websites as of January 2015.[6] WordPress is the most popular blogging system in use on the Web,[7] at more than 60 million websites.[8]

It was released on May 27, 2003, by its founders, Matt Mullenweg[1] and Mike Little,[9] as a fork of b2/cafelog. The license under which WordPress software is released is the GPLv2 (or later) from the Free Software Foundation.[10]

Contents

    1 Overview
        1.1 Themes
        1.2 Plugins
        1.3 Mobiles
        1.4 Other features
    2 Multi-user and multi-blogging
    3 History
        3.1 Release history
    4 Future
    5 Vulnerabilities
    6 Development and support
        6.1 Key developers
        6.2 WordCamp developer and user conferences
        6.3 Support
    7 See also
    8 References
    9 External links

Overview

WordPress has a web template system using a template processor.
Themes

WordPress users may install and switch between themes. Themes allow users to change the look and functionality of a WordPress website and they can be installed without altering the content or health of the site. Every WordPress website requires at least one theme to be present and every theme should be designed using WordPress standards with structured PHP, valid HTML and Cascading Style Sheets (CSS). Themes may be directly installed using the WordPress "Appearance" administration tool in the dashboard or theme folders may be uploaded via FTP.[11] The PHP, HTML (HyperText Markup Language) and CSS code found in themes can be added to or edited for providing advanced features. WordPress themes are in general classified into two categories, free themes and premium themes. All the free themes are listed in the WordPress theme directory and premium themes should be purchased from marketplaces and individual WordPress developers. WordPress users may also create and develop their own custom themes if they have the knowledge and skill to do so. If WordPress users do not have themes development knowledge then they may download and use free WordPress themes from wordpress.org.
Plugins

WordPress's plugin architecture allows users to extend the features and functionality of a website or blog. WordPress has over 40,501 plugins available,[12] each of which offers custom functions and features enabling users to tailor their sites to their specific needs. These customizations range from search engine optimization, to client portals[13] used to display private information to logged in users, to content displaying features, such as the addition of widgets and navigation bars. But not all available plugins are always abreast with the upgrades and as a result they may not function properly or may not function at all.[14]
Mobiles

Native applications exist for WebOS,[15] Android,[16] iOS (iPhone, iPod Touch, iPad),[17][18] Windows Phone, and BlackBerry.[19] These applications, designed by Automattic, allow a limited set of options, which include adding new blog posts and pages, commenting, moderating comments, replying to comments in addition to the ability to view the stats.[17][18]
Other features

WordPress also features integrated link management; a search engine–friendly, clean permalink structure; the ability to assign multiple categories to articles; and support for tagging of posts and articles. Automatic filters are also included, providing standardized formatting and styling of text in articles (for example, converting regular quotes to smart quotes). WordPress also supports the Trackback and Pingback standards for displaying links to other sites that have themselves linked to a post or an article. WordPress blog posts can be edited in HTML, using the visual editor, or using one of a number of plugins that allow for a variety of customized editing features.
Multi-user and multi-blogging

Prior to version 3, WordPress supported one blog per installation, although multiple concurrent copies may be run from different directories if configured to use separate database tables. WordPress Multisites (previously referred to as WordPress Multi-User, WordPress MU, or WPMU) was a fork of WordPress created to allow multiple blogs to exist within one installation but is able to be administered by a centralized maintainer. WordPress MU makes it possible for those with websites to host their own blogging communities, as well as control and moderate all the blogs from a single dashboard. WordPress MS adds eight new data tables for each blog.

As of the release of WordPress 3, WordPress MU has merged with WordPress.[20]
History

b2/cafelog, more commonly known as b2 or cafelog, was the precursor to WordPress.[21] b2/cafelog was estimated to have been installed on approximately 2,000 blogs as of May 2003.[22] It was written in PHP for use with MySQL by Michel Valdrighi, who is now a contributing developer to WordPress. Although WordPress is the official successor, another project, b2evolution, is also in active development.

WordPress first appeared in 2003 as a joint effort between Matt Mullenweg and Mike Little to create a fork of b2.[23] Christine Selleck Tremoulet, a friend of Mullenweg, suggested the name WordPress.[24][25]

In 2004 the licensing terms for the competing Movable Type package were changed by Six Apart, resulting in many of its most influential users migrating to WordPress.[26][27] By October 2009 the Open Source CMS MarketShare Report concluded that WordPress enjoyed the greatest brand strength of any open-source content-management system.[28]
Release history

Main releases of WordPress are codenamed after well-known jazz musicians, starting after version 1.0.[29]
Legend: 	Old version 	Older version, still supported 	Current version 	Latest preview version 	Future release
Version 	Code name 	Release date 	Notes
0.7 	none 	27 May 2003 [30] 	Used the same file structure as its predecessor, b2/cafelog, and continued the numbering from its last release, 0.6.[31] Only 0.71-gold is available for download in the official WordPress Release Archive page.
1.0 	Davis 	3 January 2004 [32] 	Added search engine friendly permalinks, multiple categories, dead simple installation and upgrade, comment moderation, XFN support, Atom support.
1.2 	Mingus 	22 May 2004 [33] 	Added support of Plugins; which same identification headers are used unchanged in WordPress releases as of 2011.
1.5 	Strayhorn 	17 February 2005 [34] 	Added a range of vital features, such as ability to manage static pages and a template/Theme system. It was also equipped with a new default template (code named Kubrick).[35] designed by Michael Heilemann.
2.0 	Duke 	31 December 2005 [36] 	Added rich editing, better administration tools, image uploading, faster posting, improved import system, fully overhauled the back end, and various improvements to Plugin developers.
2.1 	Ella 	22 January 2007 [37] 	Corrected security issues, redesigned interface, enhanced editing tools (including integrated spell check and auto save), and improved content management options.
2.2 	Getz 	16 May 2007 [38] 	Added widget support for templates, updated Atom feed support, and speed optimizations.
2.3 	Dexter 	24 September 2007 [39] 	Added native tagging support, new taxonomy system for categories, and easy notification of updates, fully supports Atom 1.0, with the publishing protocol, and some much needed security fixes.
2.5 	Brecker 	29 March 2008 [40] 	Major revamp to the dashboard, dashboard widgets, multi-file upload, extended search, improved editor, improved plugin system and more.
2.6 	Tyner 	15 July 2008 [41] 	Added new features that made WordPress a more powerful CMS: it can now track changes to every post and page and allow easy posting from anywhere on the web.
2.7 	Coltrane 	11 December 2008 [42] 	Administration interface redesigned fully, added automatic upgrades and installing plugins, from within the administration interface.
2.8 	Baker 	10 June 2009 [43] 	Added improvements in speed, automatic installing of themes from within administration interface, introduces the CodePress editor for syntax highlighting and a redesigned widget interface.
2.9 	Carmen 	19 December 2009 [44] 	Added global undo, built-in image editor, batch plugin updating, and many less visible tweaks.
3.0 	Thelonious 	17 June 2010 [45] 	Added a new theme APIs, merge WordPress and WordPress MU, creating the new multi-site functionality, new default theme "Twenty Ten" and a refreshed, lighter admin UI.
3.1 	Reinhardt 	23 February 2011 [46] 	Added the Admin Bar, which is displayed on all blog pages when an admin is logged in, and Post Format, best explained as a Tumblr like micro-blogging feature. It provides easy access to many critical functions, such as comments and updates. Includes internal linking abilities, a newly streamlined writing interface, and many other changes.
3.2 	Gershwin 	4 July 2011 [47] 	Focused on making WordPress faster and lighter. Released only four months after version 3.1, reflecting the growing speed of development in the WordPress community.
3.3 	Sonny 	12 December 2011 [48] 	Focused on making WordPress friendlier for beginners and tablet computer users.
3.4 	Green 	13 June 2012 [49] 	Focused on improvements to Theme customization, Twitter integration and several minor changes.
3.5 	Elvin 	11 December 2012 [50] 	Support for the Retina Display, color picker, new default theme "Twenty Twelve", improved image workflow.
3.6 	Oscar 	1 August 2013 [51] 	New default theme "Twenty Thirteen", admin enhancements, post formats UI update, menus UI improvements, new revision system, autosave and post locking.
3.7 	Basie 	24 October 2013 [52] 	Automatically apply maintenance and security updates in the background, stronger password recommendations, support for automatically installing the right language files and keeping them up to date.
3.8 	Parker 	12 December 2013 [53] 	Improved admin interface, responsive design for mobile devices, new typography using Open Sans, admin color schemes, redesigned theme management interface, simplified main dashboard, Twenty Fourteen magazine style default theme, second release using "Plugin-first development process".
3.9 	Smith 	16 April 2014 [54] 	Improvements to editor for media, live widget and header previews, new theme browser.
4.0 	Benny 	4 September 2014 [55] 	Improved media management, embeds, writing interface, and plugin discovery.
4.1 	Dinah 	18 December 2014 [56] 	Twenty Fifteen as the new default theme, distraction-free writing, easy language switch, Vine embeds and plugin recommendations.
4.2 	Powell 	23 April 2015 [57] 	New "Press This" features, improved characters support, emoji support, improved customizer, new embeds and updated plugin system.
4.3 	Billie 	18 August 2015 [58] 	Focus on mobile experience, better passwords and improved customizer.
4.4 	TBA 	8 December 2015 [59] 	
Future

Matt Mullenweg has stated that the future of WordPress is in social, mobile, and as an application platform.[60][61]
Vulnerabilities

Many security issues[62][63] have been uncovered in the software, particularly in 2007 and 2008. According to Secunia, WordPress in April 2009 had 7 unpatched security advisories (out of 32 total), with a maximum rating of "Less Critical."[64] Secunia maintains an up-to-date list of WordPress vulnerabilities.[65][66]

In January 2007, many high profile search engine optimization (SEO) blogs, as well as many low-profile commercial blogs featuring AdSense, were targeted and attacked with a WordPress exploit.[67] A separate vulnerability on one of the project site's web servers allowed an attacker to introduce exploitable code in the form of a back door to some downloads of WordPress 2.1.1. The 2.1.2 release addressed this issue; an advisory released at the time advised all users to upgrade immediately.[68]

In May 2007, a study revealed that 98% of WordPress blogs being run were exploitable because they were running outdated and unsupported versions of the software.[69] In part to mitigate this problem, WordPress made updating the software a much easier, "one click" automated process in version 2.7 (released in December 2008).[70] However, the filesystem security settings required to enable the update process can be an additional risk.[71]

In a June 2007 interview, Stefan Esser, the founder of the PHP Security Response Team, spoke critically of WordPress's security track record, citing problems with the application's architecture that made it unnecessarily difficult to write code that is secure from SQL injection vulnerabilities, as well as some other problems.[72]

In June 2013, it was found that some of the 50 most downloaded WordPress plugins were vulnerable to common Web attacks such as SQL injection and XSS. A separate inspection of the top-10 e-commerce plugins showed that 7 of them were vulnerable.[73]

In an effort to promote better security, and to streamline the update experience overall, automatic background updates were introduced in WordPress 3.7.[74]

Individual installations of WordPress can be protected with security plugins that prevent user enumeration, hide resources and thwart probes. Users can also protect their WordPress installations by taking steps such as keeping all WordPress installation, themes, and plugins updated, using only trusted themes and plugins,[75] editing the site's .htaccess file to prevent many types of SQL injection attacks and block unauthorized access to sensitive files. It is especially important to keep WordPress plugins updated because would be hackers can easily list all the plugins a site uses, and then run scans searching for any vulnerabilities against those plugins. If vulnerabilities are found, they may be exploited to allow hackers to upload their own files (such as a PHP Shell script) that collect sensitive information.[76][77][78]

Developers can also use tools to analyze potential vulnerabilities, including wpscan, Wordpress Auditor and Wordpress Sploit Framework developed by 0pc0deFR. These types of tools research known vulnerabilities, such as a CSRF, LFI, RFI, XSS, SQL injection and user enumeration. However, not all vulnerabilities can be detected by tools, so it is advisable to check the code of plugins, themes and other add-ins from other developers.
Development and support
Key developers

Matt Mullenweg and Mike Little were cofounders of the project. The core lead developers include Helen Hou-Sandí, Dion Hulse, Mark Jaquith, Matt Mullenweg, Andrew Ozz, and Andrew Nacin.[79][80]

WordPress is also developed by its community, including WP testers, a group of volunteers who test each release.[81] They have early access to nightly builds, beta versions and release candidates. Errors are documented in a special mailing list, or the project's Trac tool.

Though largely developed by the community surrounding it, WordPress is closely associated with Automattic, the company founded by Matt Mullenweg. On September 9, 2010, Automattic handed the WordPress trademark to the newly created WordPress Foundation, which is an umbrella organization supporting WordPress.org (including the software and archives for plugins and themes), bbPress and BuddyPress.
WordCamp developer and user conferences
A WordCamp in Sofia, Bulgaria (2011)

"WordCamp" is the name given to all WordPress-related gatherings, both informal unconferences and more formal conferences.[82] The first such event was WordCamp 2006 in August 2006 in San Francisco, which lasted one day and had over 500 attendees.[83][84] The first WordCamp outside San Francisco was held in Beijing in September 2007.[85] Since then, there have been over 350 WordCamps in over 150 cities in 48 different countries around the world.[85] WordCamp San Francisco, an annual event, remains the official annual conference of WordPress developers and users.[86][87]
Support


Mullenweg, Matt. "WordPress Now Available". WordPress. Retrieved 2010-07-22.
"WordPress 4.3.1 Security and Maintenance Release “Billie”". Retrieved 16 September 2015.
"WordPress 4.4 Beta 1". WordPress.org. WordPress Foundation. Retrieved 22 October 2015.
"WordPress: About: GPL". WordPress.org. Retrieved 15 June 2010.
"WordPress Web Hosting". WordPress. Retrieved 21 May 2013.
"Usage Statistics and Market Share of CoiPod Touch. 2008-07-12.
"18 Million WordPress Blogs Land on the iPad". ReadWriteWeb. March 24, 2011.
"WordPress for BlackBerry". WordPress. Retrieved 2009-12-27.
"WordPress 3.0 "Thelonious"". WordPress.org. 2010-06-17. Retrieved 2011-12-18.
Andrew Warner, Matt Mullenweg (2009-09-10). The Biography Of WordPress – With Matt Mullenweg (MPEG-4 Part 14) (Podcast). Mixergy. Event occurs at 10:57. Retrieved 2009-09-28. "b2 had actually, through a series of circumstances, essentially become abandoned."
Valdrighi, Michel. "b2 test weblog - post dated 23.05.03". Retrieved 9 May 2013.
"History - WordPress Codex". WordPress.org. Retrieved 29 March 2012.
Silverman, Dwight (24 January 2008). "The importance of being Matt". Houston Chronicle. Retrieved 14 August 2014.
Tremoulet, Christine Selleck (24 January 2008). "The Importance of Being Matt…". Christine Selleck Tremoulet. Retrieved 29 March 2012.
Manjoo, Farhad (9 August 2004). "Blogging grows up". Salon. Retrieved 29 March 2012.
Pilgrim, Mark (14 May 2004). "Freedom 0". Mark Pilgrim. Archived from the original on 10 April 2006. Retrieved 29 March 2012.
"2009 Open Source CMS Market Share Report, page 57, by water&stone and CMSWire Oct, 2009". CMSWire. 2009-12-17. Retrieved 2010-06-15.
"Roadmap". Blog. WordPress.org. Retrieved 2010-06-15.
"WordPress Blog: WordPress 0.7". WordPress.org. Retrieved 2003-05-27.
"Cafelog". Retrieved 2011-05-15.
"WordPress Blog: WordPress 1.0". WordPress.org. Retrieved 2004-01-03.
"WordPress Blog: WordPress 1.2". WordPress.org. Retrieved 2004-05-22.
"WordPress Blog: WordPress 1.5". WordPress.org. Retrieved 2005-02-17.
"Kubrick at Binary Bonsai". Binarybonsai.com. Retrieved 2010-06-15.
"WordPress Blog: WordPress 2.0". WordPress.org. Retrieved 2005-12-31.
"WordPress Blog: WordPress 2.1". WordPress.org. Retrieved 2007-01-22.
"WordPress Blog: WordPress 2.2". WordPress.org. Retrieved 2007-05-16.
"WordPress Blog: WordPress 2.3". WordPress.org. Retrieved 2007-09-24.
"WordPress Blog: WordPress 2.5". WordPress.org. Retrieved 2008-03-29.
"WordPress Blog: WordPress 2.6". WordPress.org. Retrieved 2008-06-15.
"WordPress Blog: WordPress 2.7". WordPress.org. Retrieved 2008-12-11.
"WordPress Blog: WordPress 2.8". WordPress.org. Retrieved 2009-06-10.
"WordPress Blog: WordPress 2.9". WordPress.org. Retrieved 2009-12-19.
"WordPress Blog: WordPress 3.0". WordPress.org. Retrieved 2010-06-17.
"WordPress Blog: WordPress 3.1". WordPress.org. Retrieved 2011-02-25.
"WordPress Blog: WordPress 3.2". WordPress.org. Retrieved 2011-06-04.
"WordPress Blog: WordPress 3.3". WordPress.org. Retrieved 2011-12-12.
"WordPress Blog: WordPress 3.4". WordPress.org. Retrieved 2012-06-13.
"WordPress Blog: WordPress 3.5". WordPress.org. Retrieved 2012-12-11.
"WordPress Blog: WordPress 3.6". WordPress.org. Retrieved 2013-08-01.
"WordPress Blog: WordPress 3.7". WordPress.org. Retrieved 2013-10-24.
"WordPress Blog: WordPress 3.8". WordPress.org. Retrieved 2013-12-12.
"WordPress Blog: WordPress 3.9". WordPress.org. Retrieved 2014-04-16.
"WordPress Blog: WordPress 4.0". WordPress.org. Retrieved 2014-09-04.
"WordPress Blog: WordPress 4.1". WordPress.org. Retrieved 2014-12-18.
"WordPress 4.2 "Powell"". WordPress.org. 2015-04-23. Retrieved 2015-04-25.
"WordPress 4.3 "Billie"". WordPress.org. Retrieved 2015-08-18.
"Version 4.4 Project Schedule". WordPress.org. Retrieved 2015-09-14.
"Radically Simplified WordPress". Ma.tt. Retrieved 2015-03-11.
"Matt Mullenweg: State of the Word 2013". Wordpress.tv. Retrieved 2015-03-11.
"Wincent Colaiuta". Wincent.com. 2007-06-21. Retrieved 2015-03-11.
"David Kierznowski". Blogsecurity.net. 2007-06-28. Retrieved 2015-03-11.
"Secunia Advisories for WordPress 2.x". Secunia.com. 2009-04-07. Retrieved 2015-03-11.
"Secunia WordPress 2.x Vulnerability Report". Secunia.com. Retrieved 2010-06-15.
"Secunia WordPress 3.x Vulnerability Report". Secunia.com. Retrieved 2010-12-27.
"WordPress Exploit Nails Big Name Seo Bloggers". Threadwatch.org. Retrieved 2011-12-18.
"WordPress 2.1.1 dangerous, Upgrade to 2.1.2". WordPress.org. 2 March 2007. Retrieved 2007-03-04.
"Survey Finds Most WordPress Blogs Vulnerable". Blog Security. 2007-05-23. Retrieved 2010-06-15.
"Updating WordPress". WordPress Codex. Retrieved 2012-09-25.
"Yet another WordPress release". 2009-08-13. Retrieved 2012-09-24.
"Interview with Stefan Esser". BlogSecurity. 2007-06-28. Retrieved 2010-06-15.
Robert Westervelt (2013-06-18). "Popular WordPress E-Commerce Plugins Riddled With Security Flaws - Page: 1". CRN. Retrieved 2015-03-11.
"Configuring Automatic Background Updates « WordPress Codex". Codex.wordpress.org. Retrieved 2014-06-30.
Ward, Simon (9 July 2012). "Original Free WordPress Security Infographic by Pingable". Pingable. Retrieved 28 October 2012.
"How To Scan Wordpress Like A Hacker".
"How To Manually Update Wordpress Plugins".
"Top 5 WordPress Vulnerabilities and How to Fix Them". eSecurityPlanet.com. 2012-04-20. Retrieved 2012-04-20.
"About WordPress". wordpress.org. Retrieved 2015-03-18.
"Core Team". codex.wordpress.org. Retrieved 2015-08-27.
"Installing WordPress". August 2014.
"WordCamp Central". Central.wordcamp.org. 2011-12-12. Retrieved 2011-12-18.
"WordCamp 2006". 2006.wordcamp.org. Retrieved 2011-12-18.
"WordCamp 2011". 2011.sf.wordcamp.org. Retrieved 2011-12-18.
"WordCamp Central > Schedule". Central.wordcamp.org. Retrieved 2011-12-18.
"WordCamp SF Announced (not WordCon) | WordCamp Central". Central.wordcamp.org. 2011-01-24. Retrieved 2015-03-11.
"Most Frequently Used Free WordPress Plugins 2014". ThemesRefinery. 2014-06-20. Retrieved 2015-03-11.
"WordPress Codex". WordPress.org. Retrieved 2014-03-13.

    "WordPress Forums". WordPress.org. Retrieved 2014-03-13.

External links
Find more about
WordPress
at Wikipedia's sister projects
Search Commons 	Media from Commons
Search Wikibooks 	Textbooks from Wikibooks
Search Wikiversity 	Learning resources from Wikiversity
Search Wikidata 	Data from Wikidata

    Official website
    WordPress at DMOZ

[hide]

    v t e 

Web application frameworks
ASP.NET 	

    ASP.NET Dynamic Data ASP.NET MVC ASP.NET Web Forms BFC DotNetNuke MonoRail OpenRasta Umbraco 

ColdFusion 	

    CFWheels ColdBox Platform ColdSpring Fusebox Mach-II Model-Glue 

Common Lisp 	

    Caveman2 CL-HTTP UnCommon Web Weblocks 

C++ 	

    CppCMS Wt 

Haskell 	

    Happstack Yesod Snap 

Java 	

    AppFuse Flexive Grails GWT ICEfaces ItsNat JavaServer Faces JHipster Jspx Makumba OpenXava Play Reasonable Server Faces Remote Application Platform RIFE Seam Spring Stripes Struts Tapestry Vaadin WebWork Wicket WaveMaker ZK 

JavaScript 	

    Ample SDK AngularJS Backbone.js Chaplin.js Closure Dojo Toolkit Ember.js Ext JS jQuery Meteor MooTools Node.js Prototype Rico script.aculo.us Sencha Touch SproutCore Wakanda 

Perl 	

    Catalyst Dancer Mason Maypole Mojolicious WebGUI 

PHP 	

    CakePHP CodeIgniter Drupal eZ Publish Fat-Free FuelPHP Horde Joomla! Kohana Laravel Lithium Midgard MODX Nette Framework Phalcon PRADO Qcodo Silex SilverStripe Symfony TYPO3 WordPress Xaraya XOOPS Yii Zend Framework 

Python 	

    BlueBream CherryPy Django Flask Grok Nevow Pyjamas Pylons Pyramid Quixote TACTIC Tornado TurboGears web2py Webware Zope 2 

Ruby 	

    Camping Merb Padrino Ruby on Rails Sinatra 

Scala 	

    Lift Play Scalatra 

Smalltalk 	

    AIDA/Web Seaside 

Other languages 	

    Application Express (PL/SQL) Grails (Groovy) Kepler (Lua) OpenACS (Tcl) SproutCore (JavaScript/Ruby) SymbolicWeb (Clojure) Yaws (Erlang) 

Authority control 	

    WorldCat VIAF: 182949223 LCCN: no2008101152 GND: 4841028-7 

Categories:

    2003 softwareBlog softwareContent management systemsSoftware forksFree content management systemsFree software programmed in PHPWordPressWebsite managementSoftware using the GPL license

Navigation menu

    Create account
    Not logged in
    Talk
    Contributions
    Log in

    Article
    Talk

    Read
    Edit
    View history

    Main page
    Contents
    Featured content
    Current events
    Random article
    Donate to Wikipedia
    Wikipedia store

Interaction

    Help
    About Wikipedia
    Community portal
    Recent changes
    Contact page

Tools

    What links here
    Related changes
    Upload file
    Special pages
    Permanent link
    Page information
    Wikidata item
    Cite this page

Print/export

    Create a book
    Download as PDF
    Printable version

Languages

    العربية
    বাংলা
    Bân-lâm-gú
    Беларуская
    Беларуская (тарашкевіца)‎
    Български
    Català
    Čeština
    Cymraeg
    Dansk
    Deutsch
    Eesti
    Ελληνικά
    Español
    Esperanto
    Euskara
    فارسی
    Français
    Frysk
    Galego
    ગુજરાતી
    한국어
    Հայերեն
    हिन्दी
    Hrvatski
    Bahasa Indonesia
    Italiano
    עברית
    Basa Jawa
    ಕನ್ನಡ
    ქართული
    Қазақша
    Latviešu
    Lietuvių
tlassian.com/download/attachments/304578655/git-tutorial-basics-clone-repotorepocollaboration.png)

Git's ability to communicate with remote repositories is the foundation of every Git-based collaboration workflow. To learn more about Git and Git workflows see, [Atlassian's Git site](https://www.atlassian.com/git/).


- - -
## SourceTree 
Download, install and configure SourceTree. Source tree is Atlassian's Git GUI client and one of the most popular on the market. 
 
This section contains the following tasks: 

* [Install and configure SourceTree](#markdown-header-install-and-configure-sourcetree): Download, install, and configure SourceTree. 
* [Clone repository locally](#markdown-header-clone-repository-locally): Learn how to clone the bucket-o-sand to your local system. 
	* [Clone from SourceTree welcome wizard](#markdown-header-clone-from-sourcetree-welcome-wizard)
	* [Clone from Bitbucket](#markdown-header-clone-from-bitbucket)
* [Inspect your repository](#markdown-header-inspect-your-repository)
	* [Repository in SourceTree](#markdown-header-repository-in-sourcetree)
	* [Repository in Bitbucket](#markdown-header-repository-in-bitbucket)
* [Make a commit and push a change](#markdown-header-make-a-commit-and-push-a-change)


####Install and configure SourceTree

1. Go to [SourceTree](http://sourcetreeapp.com) and click **Download SourceTree Free**
2. Open the downloaded file and click **Run**
3. Accept the default settings by clicking **Next** at each screen. (Windows only: Follow the instructions for installing .Net Framework if prompted.)
4. Select a location to install SourceTree or accept the default. 
5. Click **Finish** (Mac) or **Install** (Windows).

The SourceTree welcome wizard opens. Select **I agree to the license agreement** and click **Continue**. 

![Welcome wizard open page](https://confluence.atlassian.com/download/attachments/304578655/soucetree-welcomewiz-screen1.png)
 
Fantastic! Now you have SourceTree installed! Next you'll complete SourceTree configuration. 

**Configure SourceTree**

* SourceTree will install Git, if you do not already have a version installed. 

![Git install window](https://confluence.atlassian.com/download/attachments/304578655/sourcetree-install-git.png)

The welcome wizard will help you add an account. 

* Select Bitbucket in the Account **A** section, and enter the same login information you use for your Bitbucket account in the Username and Password fields **B**. 

![Set up account window](https://confluence.atlassian.com/download/attachments/304578655/sourcetree-setup-account.png)

* Click **No** to decline SSH keys for the moment. If you have SSH keys you can add them later. 

Finally, you can [Clone from SourceTree welcome wizard](#markdown-header-clone-from-sourcetree-welcome-wizard) in the next section. 




-----

[source](https://www.techdrag.xyz/2015/10/26/wordpress-adsense-system-review-how-to-setup-an-adsense-blog-step-by-step/)


##Clone repository locally
Next we'll get the repository to your local system. The git clone copies an existing Git repository as its own full-fledged Git repository. Cloning also creates a remote connection called origin pointing back to the original repository.

![Git clone](https://confluence.atlassian.com/download/attachments/304578655/git-tutorial-basics-clone.png)

###Clone from SourceTree welcome wizard
If your installing SourceTree you can clone your repository during the configuration process. 

1. Select your repository **1** from the list in the welcome wizard. 
2. Confirm the destination folder **2**, or select a new one. 
3. Click **Ok**. 

![welcome wizard clone dialog](https://confluence.atlassian.com/download/attachments/304578655/sourcetree-setup-selectrepo.png)

Fantastic!! Now you have a clone of the repository on your local system and are ready to work. Next, you can [Inspect your repository](#markdown-header-inspect-your-repository)

###Clone from Bitbucket
Learn how to clone a repository starting from Bitbucket to your local system using SourceTree.

1. Open the sidebar navigation by clicking on the **>>** symbol.
2. Click **Clone** *A*, then click **Clone in SourceTree** *B*.  

![Clone process](https://confluence.atlassian.com/download/attachments/304578655/tutorial-clone-sourcetree1.png)

**NOTE** For Mac you will be asked to launch an application, click **OK** to continue.

This will prompt SourceTree to open the clone dialog, as shown in the following example: 

* Microsoft Windows **A**  
* Macintosh **B** 

![Clone dialog](https://confluence.atlassian.com/download/attachments/304578655/tutorial-clone-step2.png)

Check the destination path and modify it if you wish to place your repository file in a different directory. 

You might want to create a directory specifically for your repositories: 
	/repositories/bucket-o-sand 
	


----
##Inspect your repository
Let's take a look around your repository in both Bitbucket and SourceTree.

####Repository in SourceTree
The files and titles in your repository might be a bit different, but the basics are the same. 

![Looking at the repository in SourceTree](https://confluence.atlassian.com/download/attachments/304578655/sourcetree_gui.png)

**1** Repository bookmarks: displays a list of all the repositories you have listed. You can double click a bookmarked repository to open it in an active tab.

**2** Active repository tab: you can have many repositories open at once, each tab lets you view a different repository. In this example *working copy* is selected, so in the next pane you can see a list of the files in the working copy of this repository. 

**3** View selection: in this pane *working copy* you can choose to view only the files in a particular state (such as: All, Modified, Clean). 

**4** Working copy and Staged changes: here you can see the files in the active view (based on the selection in 3) and drag changed files from working into staged to create a change set to commit.

**5** Currently selected file: you can see the diff view of a selected file in the working copy or staged area. 

####Repository in Bitbucket
Your repository will have some differences but, again, the basics are the same. The view in the following example has the sidebar expanded press [ to expand the sidebar. 

![Looking at a repository in Bitbucket](https://confluence.atlassian.com/download/attachments/304578655/tutorial-sandbucket-bitbucket-reposview.png?)

**A** Actions: all the most common actions are here, create a clone, branch, pull request, etc... 

**B** Navigation: this is where you can get to all the things in Bitbucket (such as: source code, list of commits, list of branches). 

**C** README: The view in the preceding example is on the *Overview* page where you can configure your own README using markdown or plain text.

**D** Recent activity: lists the most recent commits, pushes, merges, and pull request activity. 

----
##Make a commit and push a change 
Now your ready to make a change, add that change to your local repository, and push the change to your remote Bitbucket repository.

####Open a file from SourceTree
A simple thing like selecting and opening a file from the source control UI can make things move quicker. 

1. Select **All** from view selection menu. 
2. Select the 'sample.html' file, then right click and select **Show in Explorer** (Windows). Or click *...* then select **Show in Finder** (Mac)
3. Using your favorite editor, edit the `sample.html` file.
4. Change the heading from *My First File* to *Playing in the Sand*.
5. Save and close the file. 

####Commit the change in SourceTree
Now you've made a change locally and are ready to go through the git process of adding that change to the project history locally.

The 'git add' moves changes from the working directory to the staging area. This gives you the opportunity to prepare a set of changes (a snapshot) before committing it to the official history.

![Git add command](https://confluence.atlassian.com/download/attachments/304578655/git-tutorial-basics-add.png)

The git commit takes the staged snapshot and commits it to the project history. Combined with git add, this defines the basic workflow for all Git users.

![Git commit](https://confluence.atlassian.com/download/attachments/304578655/git-tutorial-basics-commit.png)

Let's do that in SourceTree
![Modified file in SourceTree](https://confluence.atlassian.com/download/attachments/304578655/tutorial-sourcetree-changedfile.png)

1. Identify the changed file **A** in SourceTree by noting the change in color and from a checkmark to an ellipsis, as shown in the following example. 
2. Click and drag the `sample.html` **A** that file to the *Staged files* **B** area. This action is the same as the 'git add' or 'git stage' command. 
3. Click **Commit** from the [actions menu](#markdown-header-push-the-change-to-bitbucket) in SourceTree. 
4. Add a commit message **C**, then click **Commit**.

Great! Now the snapshot of your change has been added to your local project history. 

####Push the change to Bitbucket 
The last thing we'll do is push that change to Bitbucket. 
 
![SouceTree toolbar with one item highlighted in the push action](https://confluence.atlassian.com/download/attachments/304578655/sourcetree-toolbar-push1.png)

Click **Push** from the toolbar. You'll notice there is a *1* highlighted in the **Push** action. This is the number of commits ready to be pushed to the remote repository, Bitbucket in this case. 

The push dialog opens. Review the settings and click **Ok**.  

![SourceTree push dialog](https://confluence.atlassian.com/download/attachments/304578655/sourcetree-push-dialog.png)

Pushing lets you move a local branch or series of commits to another repository, which serves as a convenient way to publish contributions. This is like svn commit, but it sends a series of commits instead of a single changeset.

![Push graphic](https://confluence.atlassian.com/download/attachments/304578655/git-tutorial-remote-repositories-push.png)

Congratulations! You've done all the basics! Feel free to use the tutorial repository to learn more, test, experiment, and expand your knowledge. You can jump into the [Bitbucket 101](https://confluence.atlassian.com/x/cgozDQ) after the *Clone your repository* section. Or you can check out [Atlassian's Git site](https://www.atlassian.com/git/) and learn more about Git workflows. 

All of us at Bitbucket and SourceTree hope your experience is a great one! We are constantly working and building a better Bitbucket and SourceTree. 

- - -

#Command line
  
Learn the very basics of cloning, committing, and pushing from the command line. Git is a very effective from the command line and it's commands are reasonably easy to learn.

**If you don't already have Git installed** on your local system see, [Set up Git](https://confluence.atlassian.com/x/V4DHHw), then return here. 

**This section contains the following tasks: 

* [Clone your repository](#markdown-header-clone-your-repository)
* [Stage, commit, and push a change](#markdown-header-make-a-commit-and-push-the-change)

If you are unfamiliar with the Git, or the Git commands here are a couple very good resources: 

* [Atlassian Git cheatsheet](https://www.atlassian.com/dms/wac/images/landing/git/atlassian_git_cheatsheet.pdf): Nice crisp PDF of all the basic Git commands. 
* [Atlassian's Git site](https://www.atlassian.com/git/)

### Clone your repository:

Cloning makes a local copy of the repository for you.

![Clone from command line](https://confluence.atlassian.com/download/attachments/304578655/repo-setup-clone_menu-sidexpand.png)

1. Click  **Clone** in Bitbucket **A**, as shown in the previous figure. 
2. Make sure the protocol **B** is set to HTTPS, as shown in the previous figure.

    Bitbucket pre-fills the clone command for you.
    
3. Copy the command **C**. 
4. Open a terminal, or launch a GitBash terminal, on your local machine.
5. Navigate to the directory where you want your files. Use the cd /path-to-your/directory command to navigate to the location you want your repository. 
6. Paste the command **C** you copied in step 3 at the prompt.
7. Press ENTER on your keyboard.
The result should be something like. 

Git clones your repository from Bitbucket to your local machine.
> If you have trouble cloning from these instructions you can check out the more [detailed tutorial](https://confluence.atlassian.com/x/W4DHHw).

The git clone command copies an existing Git repository as its own full-fledged Git repository with its own history, manages its own files, and is a completely isolated environment from the original repository. Cloning also creates a remote connection called origin pointing back to the original repository

![Git clone](https://confluence.atlassian.com/download/attachments/304578655/git-tutorial-basics-clone.png)


### Make a commit and push the change

Learn the Git basics of stage, commit, push when you make a change to the 'sample.html' file.

1.  Navigate to the directory (folder) where you cloned your repository.
2.  Using your favorite editor, open the `sample.html` file.
3.  Change the heading from `My First File` to `Playing in the Sand`. 
4.  Save and close the file.
5.  Navigate to the repository in your command line 'cd path/to/your-repository'.
5.  Stage the file with Git.
    
    `git add sample.html`
    
6.  Commit the change.
 
    `git commit -m "changing sample.html"`
    
7. Push to Bitbucket.

    `git push`
    
    The system prompts you for a username/password.
    
8. Enter your Bitbucket account name and the password.
9. After the push completes, click **Commits**, in Bitbucket, to view your change.

The 'git add' moves changes from the working directory to the staging area. This gives you the opportunity to prepare a set of changes (a snapshot) before committing it to the official history.

![Git add command](https://confluence.atlassian.com/download/attachments/304578655/git-tutorial-basics-add.png)

The git commit takes the staged snapshot and commits it to the project history. Combined with git add, this defines the basic workflow for all Git users.

![Git commit](https://confluence.atlassian.com/download/attachments/304578655/git-tutorial-basics-commit.png)

Pushing lets you move a local branch or series of commits to another repository, which serves as a convenient way to publish contributions. This is like svn commit, but it sends a series of commits instead of a single changeset.

![Push graphic](https://confluence.atlassian.com/download/attachments/304578655/git-tutorial-remote-repositories-push.png)

Congratulations! You've done all the basics! Feel free to use the tutorial repository to learn more, test, experiment, and expand your knowledge. You can jump into the [Bitbucket 101](https://confluence.atlassian.com/x/cgozDQ) after the *Clone your repository* section. Or you can check out [Atlassian's Git site](https://www.atlassian.com/git/) and learn more about Git workflows. 

All of us at Bitbucket and SourceTree hope your experience is a great one! We are constantly working and building a better Bitbucket and SourceTree.

----

[lexers]: http://pygments.org/docs/lexers/
[fireball]: http://daringfireball.net/projects/markdown/ 
[Pygments]: http://www.pygments.org/ 
[Extra]: http://michelf.ca/projects/php-markdown/extra/
[id]: http://example.com/  "Optional Title Here"
[BBmarkup]: https://confluence.atlassian.com/x/xTAvEw